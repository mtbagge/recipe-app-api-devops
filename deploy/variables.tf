variable "prefix" {
  type        = string
  default     = "raad"
  description = "naming prefix"
}

variable "project" {
  type    = string
  default = "recipe-app-api-devops"
}

variable "contact" {
  type    = string
  default = "mtbagge@gmail.com"
}


